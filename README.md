# grap-ocabot-env

## Local installation & run via ngrok

- sudo snap install ngrok

- login https://ngrok.com

- follow steps : https://dashboard.ngrok.com/get-started/setup

- run ``./install.sh``

- ``cd src/oca-github-bot/``

- create a token for the github account ``github-grap-bot-ngrok-dev``
  with the following access:
    - repo
    - admin:org
    - admin:repo_hook
    - admin:org_hook
    - write:discussion
- enter information in the ``environment`` file

## EACH TIME

- ``docker-compose up --build``

- once launch, check that ``http://localhost:8080`` answer. (405: Method Not Allowed)

# for each new development.

- run ``ngrok http 8080``

- copy the forwarding url something like ``https://xxxx.ngrok.io``

- create a webhook on an organization, on github and add the corresponding url.
  (exemple : https://github.com/organizations/grap-org-test-bot/settings/hooks/366351048)

# Run test

virtualenv env --python=python3.8
. ./env/bin/activate

pip install -e .
pip install -r requirements-test.txt
pytest --cov=oca_github_bot