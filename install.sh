sudo apt-get install -y docker-compose
sudo apt-get install -y python3-venv

sudo usermod -a -G docker $USER

# Install gitaggregator out
pipx install git-aggregator

# get Code
gitaggregate -c repos.yml 

cp ./src/oca-github-bot/environment.sample ./src/oca-github-bot/environment

# TODO, update environment file

